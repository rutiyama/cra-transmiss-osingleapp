/**
 * ServerCra.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.cra21.crapr;

public interface ServerCra extends javax.xml.rpc.Service {
    public java.lang.String getServerCraPortAddress();

    public br.com.cra21.crapr.ServerCraPortType getServerCraPort() throws javax.xml.rpc.ServiceException;

    public br.com.cra21.crapr.ServerCraPortType getServerCraPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
