/**
 * ServerCraPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.cra21.crapr;

public interface ServerCraPortType extends java.rmi.Remote {
    public java.lang.String remessa(java.lang.String userArq) throws java.rmi.RemoteException;
    public java.lang.String confirmacao(java.lang.String userArq, java.lang.String userDados) throws java.rmi.RemoteException;
    public java.lang.String retorno(java.lang.String userArq, java.lang.String userDados) throws java.rmi.RemoteException;
    public java.lang.String desistencia(java.lang.String userArq) throws java.rmi.RemoteException;
    public java.lang.String cancelamento(java.lang.String userArq) throws java.rmi.RemoteException;
    public java.lang.String buscarApresentante() throws java.rmi.RemoteException;
}
