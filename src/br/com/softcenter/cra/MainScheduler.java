package br.com.softcenter.cra;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

import br.com.softcenter.cra.controller.RunScheduler;

public class MainScheduler {
	
	public MainScheduler() throws NoSuchAlgorithmException, UnsupportedEncodingException{
		
//		ServerSocketConnection.getSocketConn().start();
		
		Logger.getLogger("br.com.softcenter.cra").info("Iniciando Serviço");
		RunScheduler scheduler = new RunScheduler();
		scheduler.execute();
	}
	
	public static void main (String args[]) throws NoSuchAlgorithmException, UnsupportedEncodingException{
		new MainScheduler();
	}
}
