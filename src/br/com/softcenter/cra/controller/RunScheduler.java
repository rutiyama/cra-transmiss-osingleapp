package br.com.softcenter.cra.controller;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.softcenter.cra.controller.enums.ConsultarServico;
import br.com.softcenter.cra.controller.servicosdisponiveis.ServicoFactory;
import br.com.softcenter.cra.controller.servicosdisponiveis.Servicos;
import br.com.softcenter.cra.model.beans.BeanComarcasWS;
import br.com.softcenter.cra.model.dao.impl.ComarcasWSDAOImpl;
import br.com.softcenter.cra.model.dao.impl.DAOModel;
import br.com.softcenter.cra.utils.date.DateUtils;
import br.com.softcenter.cra.utils.xml.CRAException;
import br.com.softcenter.cra.utils.xml.Remessa.XMLLogRemessa;

/**
 * @author Rodrigo Junior Utiyama
 * @since 01-09-2015
 */

public class RunScheduler {
	
	private Logger logger;
	
	public void execute()  {
		this.logger = Logger.getLogger("br.com.softcenter.cra.controller");

		//TODO criar verificação se o arquivo log já existe, senão, criar uma nova.
		//TODO também criar validação se o log a ser gerado é após a data da do último registro no log
			try{ 
//				System.out.println(XMLLogRemessa.UltimaVerificacaoDoArquivoTemp());
				
				/*
				 * Se o arquivo temp já existe, verifica se não está vazio. Se não estiver vazio,
				 * recupera a data da última verificação no temp.cra e verifica se é do mesmo dia:
				 *  - Se a data da última verificação for do mesmo dia, significa que o usuário ainda 
				 *  está verificando;
				 *  
				 *  - Se a data não for do mesmo dia, significa que é de outro dia, portanto, zera o 
				 *  arquivo temp.cra.
				 */
				if(XMLLogRemessa.ArquivoTempExiste()){
					
					if(!XMLLogRemessa.ArquivoTempEstaVazio()){
						logger.info("Arquivo temp.cra já existente, verificando a última consulta...");
						Date ultimaVerificacao = XMLLogRemessa.UltimaVerificacaoDoArquivoTemp();
						
						if(!DateUtils.isSameDate(ultimaVerificacao, Calendar.getInstance().getTime())){
							
							logger.info("A última consulta realizada em " + DateUtils.ConvertDateUtilToFormatedString(ultimaVerificacao) + 
							". Resetando arquivo temp.cra para a verificação de hoje ("+DateUtils.ConvertDateUtilToFormatedString(new Date()) + ").");
							
							XMLLogRemessa.CriarArquivoLOG();
						}
					}
					
				}else{
					logger.info("Arquivo de log não existe! Criando na pasta raiz do serviço!");
					XMLLogRemessa.CriarArquivoLOG();
				}
			}catch(Exception e){
				logger.error(e.getMessage());
			}
		
		//Será verificado, todos os dias, cada uma das comarcas:
		for(BeanComarcasWS comarca : retornarComarcas()){
			iniciarVerificacao(comarca, ConsultarServico.REMESSA);
		}
		
	} 
	
	private synchronized void iniciarVerificacao(BeanComarcasWS comarca, ConsultarServico tipoDoServico){	
		/*
		 * Atualmente, passando o tipo de serviço como REMESSA. Instanciará um objeto
		 * do tipo ServicoRemessa (padrão factory):
		 */
		Servicos<Object, Object> servico = ServicoFactory.getInstance(tipoDoServico); 
		
		try{
			servico.validarServico(comarca);
		}catch(CRAException rem){
//			ServerSocketConnection.getSocketConn().sendToAll(rem.getMessage());
			logger.error(rem.getMessage());
		}catch(Exception e){
			logger.error(e.getMessage());
		}
	}
	
	private List<BeanComarcasWS> retornarComarcas(){
		DAOModel<BeanComarcasWS, Integer> comarcasDAO = new ComarcasWSDAOImpl();
		return comarcasDAO.select();
		
//		List<BeanComarcasWS> list = new ArrayList<BeanComarcasWS>();
//		list.add(new BeanComarcasWS("1", "01distpontagrossa","pontagrossa", "/home/rodrigo/Documentos/pg/"));
//		list.add(new BeanComarcasWS("2", "distcampolargo","01distcampolargo", "/home/rodrigo/Documentos/t/"));
//		return list;
	}
	
}
