package br.com.softcenter.cra.controller.enums;

public enum XMLTagsEnum {
	NODE_REMESSA("remessa"),
	NODE_ARQUIVO("nome_arquivo"),
	NODE_HEADER ("hd"),
	NODE_TRANSACAO("tr"),
	NODE_TRAILLER("tl"),
	CHILD_RELATORIO_OCORRENCIA("ocorrencia"),
	IDENTIFICADOR_CODIGO_ERRO("codigo");
	
	private String tag;
	
	private XMLTagsEnum(String tag){
		this.tag = tag;
	}
	
	public String value(){ 
		return this.tag;
	}
}
