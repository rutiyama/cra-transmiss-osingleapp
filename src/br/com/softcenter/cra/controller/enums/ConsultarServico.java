package br.com.softcenter.cra.controller.enums;

public enum ConsultarServico {
	REMESSA(1), BUSCAR_APRESENTANTES(2);
	
	private int indice;
	
	private ConsultarServico(int indice){
		this.indice = indice;
	}
}
