package br.com.softcenter.cra.controller.servicosdisponiveis;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import br.com.softcenter.cra.controller.enums.RemessaEnum;
import br.com.softcenter.cra.model.beans.BeanComarcasWS;
import br.com.softcenter.cra.model.beans.BeanPortador;
import br.com.softcenter.cra.model.dao.connection.ConexaoSoap;
import br.com.softcenter.cra.model.dao.impl.DAOModel;
import br.com.softcenter.cra.model.dao.impl.PortadorDAOImpl;
import br.com.softcenter.cra.utils.date.DateUtils;
import br.com.softcenter.cra.utils.xml.CRAException;
import br.com.softcenter.cra.utils.xml.Remessa.XMLReaderRemessa;
import br.com.softcenter.cra.utils.xml.Remessa.XMLRemessaExport;

/**
 * @author Rodrigo Junior Utiyama
 * @since 01-09-2015
 */

public class ServicoRemessa implements Servicos <BeanComarcasWS, RemessaEnum>{
	private XMLReaderRemessa XMLReader;
	private static final String SIGLA = "B";
	
	@Override
	//Verifica se o arquivo existe, retornando o código de erro:
	public RemessaEnum verificar(String conteudoXML) throws Exception {
		//Inicia o parsing do conteúdo:
		XMLReader = new XMLReaderRemessa(conteudoXML);
		XMLReader.iniciarParsingXML();
		
		return XMLReader.getCodigoOcorrencia();
	}

	@Override
	public String retornarConteudo(String nomeGenericoArquivo, String usuario, String senha) throws MalformedURLException, RemoteException {
		return ConexaoSoap.getServerConnection(usuario, senha).remessa(nomeGenericoArquivo);
	}

	@Override
	public void exportar(BeanComarcasWS comarca) throws CRAException, Exception{
		new XMLRemessaExport(XMLReader.getRemessaList()).exportar(comarca);
	}
	
	/**
	 * Faz consulta ao serviço SOAP, que retorna o XML. Depois realiza uma verificação do conteúdo
	 * para validar se o conteúdo retornado é um aviso de erro ou se é um arquivo de remessa.
	 */
	@Override
	public void validarServico(BeanComarcasWS comarca) throws Exception, CRAException{

		Logger logger = Logger.getLogger("br.com.softcenter.cra.controller.servicosdisponiveis");
		String nomeGenericoArquivo = montarNomeGenericoSomenteParaConsulta();

		logger.info("Consultando arquivo: " + nomeGenericoArquivo + " com o usuário " + comarca.getLogin());
		//Realiza a consulta do arquivo (no caso, Remessa), retornando o conteúdo do SOAP:
		String conteudo = retornarConteudo(nomeGenericoArquivo, 
				comarca.getLogin(), comarca.getSenha());
	
		//Verifica o conteúdo retornado, se há um código de erro ou não:
		RemessaEnum valicacao = verificar(conteudo);
		switch(valicacao){
			case FALHA_AUTENTICACAO:
			case ERRO_GERAL:
			case INSTITUICAO_INATIVA:
			case NOME_ARQUIVO_INVALIDO:
			case REMESSA_INEXISTENTE:  throw new CRAException(comarca.getLogin() + ": "+ valicacao.descricaoDoErro());
			case SUCESSO: exportar(comarca); break;
		}
	}
	
	/**
	 * Monta o nome do arquivo de forma genérica, pois para consultar no serviço, é necessário apenas
	 * informar a sigla e a data de consulta. O número do apresentante e a sequência não serão considerados. 
	 * Basta informar: Sigla+000+DataDoArquivo.1, Exemplo: B0000111.151 -> Todas as remessas de 01/11/2015, 
	 * de todos os apresentantes.
	 * @return Todas as remessas de todos os apresentantes da data atual.
	 */
	@Override
	public String montarNomeGenericoSomenteParaConsulta(){
		Date data = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.format(data);
//		try {
//			data = sdf.parse("2015-10-06");
//		} catch (ParseException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		return (SIGLA+"000"+DateUtils.ConvertDateUtilsToCRADate(data)+"1");
	}
	
	public List<BeanPortador> retornarPortadores(){
		DAOModel<BeanPortador, Integer> dao = new PortadorDAOImpl();
		return dao.select();
	}
	
}
