package br.com.softcenter.cra.controller.servicosdisponiveis;

import br.com.softcenter.cra.controller.enums.ConsultarServico;

public class ServicoFactory {
	private static Servicos servico = null;
	
	
	public static Servicos getInstance(ConsultarServico tipoDoServico){
		switch(tipoDoServico){
			case REMESSA: 			   servico = instanciaRemessa(); 			break;
			case BUSCAR_APRESENTANTES: servico = instanciaBuscarApresentante(); break;
			default: 				   servico = instanciaRemessa();
		}
		return servico;
	}

	private static Servicos instanciaRemessa(){
		if(servico instanceof ServicoRemessa)
			return servico;

		return new ServicoRemessa();
	}
	
	private static Servicos instanciaBuscarApresentante(){
		if(servico instanceof ServicoBuscarApresentante)
			return servico;
		
		return new ServicoBuscarApresentante();
	}
}
