package br.com.softcenter.cra.utils.security;

import java.security.MessageDigest;

public class Criptografia {
	public static boolean isSenhaCorreta(String senhaUsuarioCorreto, String senhaUsuarioDigitado){
		MessageDigest digester = null;
		try{
			digester = MessageDigest.getInstance("MD5");
		}catch(Exception ex){
			ex.printStackTrace();
		}
		return gerarHash(senhaUsuarioDigitado, digester).equals(senhaUsuarioCorreto);
	}
	
	private static String gerarHash(String frase, MessageDigest digester) {
		if (frase == null || frase.length() == 0) {
			throw new IllegalArgumentException(
					"Ocorreu um problema: senha vazia ou nula!");
		}
		
		digester.update(frase.getBytes());
		byte[] hash = digester.digest();
		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < hash.length; i++) {
			if ((0xff & hash[i]) < 0x10) {
				hexString.append("0" + Integer.toHexString((0xFF & hash[i])));
			} else {
				hexString.append(Integer.toHexString(0xFF & hash[i]));
			}
		}
		return hexString.toString();
	}
}
