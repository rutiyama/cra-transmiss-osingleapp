package br.com.softcenter.cra.utils.date;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Rodrigo Junior Utiyama
 * @since  30-09-2015
 */

public class DateUtils {
	
	public static boolean isSameDate(Date firstDate, Date secondDate){
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		
		return sdf.format(firstDate).equals(sdf.format(secondDate));
	}
	
	public static java.sql.Date ConvertUtilToSQLDate(java.util.Date data){
	     java.sql.Date sqlDate = new java.sql.Date(data.getTime());
	     return sqlDate;
	}
	
	public static String ConvertToFirebirdDateString(java.util.Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy H:mm");
		return  formatter.format(date);
	}
	
	public static String ConvertToMySQLDateString(java.util.Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		return  formatter.format(date);
	}
	
	public static java.util.Date ConvertStringToUtilDate(String data) throws ParseException{
		SimpleDateFormat formatter = new  SimpleDateFormat("yyyy-MM-dd");
		return formatter.parse(data) ;
	}
	
	public static String ConvertDateUtilToString(java.util.Date data){
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		return  formatter.format(data);
	}
	
	public static String ConvertDateUtilsToCRADate(java.util.Date data){
		SimpleDateFormat formatter = new SimpleDateFormat("ddMM.yy");
		return  formatter.format(data);
	}
	
	public static String ConvertDateUtilsToCRARemessa(java.util.Date data){
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");
		return  formatter.format(data);
	}
	
	public static String FormatDateStringToMySQL(String data){
		data =   (data.substring(4,8) + "-" + data.substring(2, 4) + "-" +  data.substring(0,2));
		return data;
	}
	
	public static String ConvertDateUtilToFormatedString(java.util.Date data){
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(data);
	}
	
	
	public static java.util.Date ConvertStringToCRADate(String data){
		try{
			DateFormat formatter = new SimpleDateFormat("ddMMyyyy");  
			return formatter.parse(data);
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
}
