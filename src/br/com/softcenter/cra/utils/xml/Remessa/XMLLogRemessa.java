package br.com.softcenter.cra.utils.xml.Remessa;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Date;
import java.util.StringTokenizer;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import br.com.softcenter.cra.model.beans.BeanComarcasWS;
import br.com.softcenter.cra.utils.date.DateUtils;

/**
 * @author Rodrigo Junior Utiyama
 * @since 01-11-2015
 */

public class XMLLogRemessa {
	
	//Caminho que será armazenado o arquivo de Log: Será o mesmo caminho do .jar
	private static final String PATH = System.getProperty("user.dir") + "/";
	
	private static Logger logger;
	
	public static void CriarArquivoLOG() throws IOException{
		logger = Logger.getLogger("br.com.softcenter.cra.utils.xml.Remessa");
		
		BufferedWriter writer = Files.newBufferedWriter(Paths.get(PATH+"temp.cra"), 
                StandardCharsets.UTF_8);
		writer.close();
		logger.info("Limpando arquivo de Log de arquivos de remessa já transferidos.");
	}
	
	public static boolean ArquivoTempEstaVazio() throws Exception{
		logger = Logger.getLogger("Arquivo temp.cra está vazio. Sendo a primeira verificação.");
		
		//Se o arquivo temp.cra já existe, verifica se está vazio:
		if (FileUtils.readFileToString(new File(PATH+"temp.cra")).trim().isEmpty()) {
			return true;
		}   

		return false;
	}
	
	public static boolean ArquivoTempExiste(){
		if (Files.exists(Paths.get(PATH+"temp.cra"), LinkOption.NOFOLLOW_LINKS))
			return true;
		
		return false;
	}
	
	public static Date UltimaVerificacaoDoArquivoTemp() throws Exception{
		BufferedReader bReader = Files.newBufferedReader(Paths.get(PATH+"temp.cra"));
		String linha = null;
		String data= null;
		String ultimaLinha = null;
		//String arquivo = null;
		StringTokenizer sTok = null;
		
		//Lê a última linha do arquvi temp.cra em busca do conjunto de linhas:
		while((linha = bReader.readLine()) != null){
			ultimaLinha = linha;
		}
		
		sTok = new StringTokenizer(ultimaLinha, ";");
		
		//Para cada token da string retornada, salva temporariamente em busca da data da última verificação:
		while(sTok.hasMoreElements()){
			data = sTok.nextToken();
		}
		
//		SimpleDateFormat sdf  = new SimpleDateFormat("dd/mm/yyyy");
//		Date d    			  = new SimpleDateFormat("ddMMyy").parse(data);
//		String dataformatada  = sdf.format(d);
		
		return DateUtils.ConvertStringToCRADate(data);
	}
	
	public synchronized static boolean ArquivoJaFoiBaixado(String pSequencialRemessaH08, String pCodigoMunicipio, String pApresentante) throws IOException{
		//1. Cria um novo Buffer para o arquivo temp.cra, para iniciar a sua leitura:
		BufferedReader bReader = Files.newBufferedReader(Paths.get(PATH+"temp.cra"));
		String linha = null;
		StringTokenizer sTok = null;
		
		String sequenciaRemessa, codigoComarca, arquivo, apresentante, dataVerificacao;
		
		//2. Inicia um loop para verificar cada linha do arquivo temporário:
		try{
			while((linha = bReader.readLine()) != null){
				//2.1 Para cada linha, é realizado uma leitura de cada elemento separado por ";":
				sTok = new StringTokenizer(linha, ";");

				while(sTok.hasMoreElements()){
				   /* 2.2: Para cada elemento, é comparado se é igual a remessa. Se for verdadeiro,
					       significa que o arquivo Remessa já foi baixado: */
					arquivo   		 = sTok.nextToken();
					sequenciaRemessa = sTok.nextToken();
					codigoComarca    = sTok.nextToken();
					apresentante     = sTok.nextToken();
					dataVerificacao  = sTok.nextToken();
					if(sequenciaRemessa.equals(pSequencialRemessaH08) && codigoComarca.equals(pCodigoMunicipio) && apresentante.equals(pApresentante))
						return true;
				}
			}
		}finally{
			bReader.close();
		}
		
		//*3: Retorna falso caso nenhum arquivo já foi baixado:
		return false;
	}
	
	public static synchronized void GravarLOG(String nomeArquivo, String sequenciaRemessaH08, BeanComarcasWS comarca, String apresentante) throws IOException{
//		byte[] linha =  ("\n"+nomeArquivo + ";" + sequenciaRemessaH08 + ";"+comarca.getCodigoMunicipio()+";" + apresentante).getBytes();
		
		//Formato da data no arquivo: ddMMyyyy
		byte[] linha =  ("\n"+nomeArquivo + ";" + 
						sequenciaRemessaH08 + ";"+
						comarca.getCodigoMunicipio()+";" + 
						apresentante + ";" + 
						DateUtils.ConvertDateUtilsToCRARemessa(new Date())).getBytes();
		
		if(Files.notExists(Paths.get(PATH+"temp.cra"), LinkOption.NOFOLLOW_LINKS))
			CriarArquivoLOG();
		
		Files.write(Paths.get(PATH+"temp.cra"), linha, StandardOpenOption.APPEND);
	}
	
	public static synchronized int RetornarSequencia(String pNomeParcialArquivo, String pCodigoMunicipio, String pApresentante) throws IOException{
		//1. Cria um novo Buffer para o arquivo temp.cra, para iniciar a sua leitura:
		BufferedReader bReader = Files.newBufferedReader(Paths.get(PATH+"temp.cra"));
		String linha = null;
		int sequencia = 1;
		int controle  = 0;
		StringTokenizer sTok = null;
		
		String sequenciaRemessa, codigoComarca, arquivo, apresentante, dataVerificacao;
		
		//2. Inicia um loop para verificar cada linha do arquivo temporário:
		try{
			while((linha = bReader.readLine()) != null){
				//2.1 Para cada linha, é realizado uma leitura de cada elemento separado por ";":
				sTok = new StringTokenizer(linha, ";");
				++controle;
				
				//2.2: Loop para cada ler o token atual:
				while(sTok.hasMoreElements()){
					/* 2.2.1: Se contêm uma sequência semelhante, é contado mais um, 
					 * indicando outra sequencia: */
					
					arquivo   		 = sTok.nextToken();
					sequenciaRemessa = sTok.nextToken();
					codigoComarca    = sTok.nextToken();
					apresentante     = sTok.nextToken();
					dataVerificacao  = sTok.nextToken();
					
//					System.out.println("ARQUIVO: " + arquivo + "\t\t " + pNomeParcialArquivo + "\nSEQUENCIA: " + sequencia + 
//							"\nCOMARCA " + codigoComarca + "\t\t " + pCodigoMunicipio + "\n" + apresentante + "\t\t" + pApresentante);
					
					if(arquivo.contains(pNomeParcialArquivo) && codigoComarca.equals(pCodigoMunicipio) && apresentante.equals(pApresentante))
						++sequencia;
					
					//2.2.2Flag para indicar que é para ler apenas o primeiro Token (nome do arquivo):
					if(controle == 1){
						controle = 0;
						break;
					}
				}
			}
		}finally{
			bReader.close();
		}
		
		return sequencia;
	}
}
