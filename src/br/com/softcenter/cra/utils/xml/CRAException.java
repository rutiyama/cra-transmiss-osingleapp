package br.com.softcenter.cra.utils.xml;

public class CRAException extends Exception{

	public CRAException(String ex) {
		super(ex);
	}
}
