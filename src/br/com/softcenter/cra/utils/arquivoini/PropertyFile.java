package br.com.softcenter.cra.utils.arquivoini;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

import br.com.softcenter.cra.model.beans.BeanIniUtils;

/**
 * 
 * @author Rodrigo Junior Utiyama
 * @since 26-08-2015
 *
 */
public class PropertyFile{

	private static PropertyFile propertyFileInstance;
	private static BeanIniUtils config = null;
	
	private PropertyFile() {
		try{
			Properties prop = new Properties();
			String propertyFile = "/cradb.properties";
			InputStream inputStream = new FileInputStream(System.getProperty("user.dir")+propertyFile);
			prop.load(inputStream);
			
			if (prop != null){
				config = new BeanIniUtils(
						prop.getProperty("PROP.DATABASE_URL"), 
						prop.getProperty("PROP.DATABASE_USER"),
						prop.getProperty("PROP.DATABASE_PASS"),
						prop.getProperty("PROP.DATABASE_ENCODING"),
						prop.getProperty("PROP.URL_SOAP"));
				
			}	
		}catch(IOException e){
			Logger.getLogger("br.com.softcenter.cra.model.dao.connection").error(e.getMessage());
		}
	}	
	
	/**
	 * @see br.com.softcenter.cra.utils.arquivoini.IniUtils#selectIni()
	 * Método que implementa a interface IniUtils, sendo responsável por retornar
	 * uma instância da classe BeanIniUtils, contendo informações do arquivo Property
	 * do Firebird:
	 */
	public synchronized static PropertyFile getProperty(){
		
		if(propertyFileInstance == null)
			propertyFileInstance = new PropertyFile();
		
		return propertyFileInstance;
	}

	public BeanIniUtils getConfig(){
		return config;
	}
}
