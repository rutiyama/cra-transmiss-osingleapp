package br.com.softcenter.cra.model.dao.impl;

import java.sql.SQLException;
import java.util.List;

/**
 * @author Rodrigo Junior Utiyama
 * @since  27-08-2015
 */
public interface DAOModel<T,t>{
	public abstract boolean insert(T usuario);
	public abstract boolean delete(T usuario);
	public abstract boolean update(T usuario, t codigo);
	public abstract T select(t codigo);
	public abstract List<T> select();
	public abstract int getLastRecordCount() throws SQLException;
}
