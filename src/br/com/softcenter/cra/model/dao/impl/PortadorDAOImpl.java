package br.com.softcenter.cra.model.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.softcenter.cra.model.beans.BeanPortador;
import br.com.softcenter.cra.model.dao.connection.FirebirdDatabaseConfig;
import br.com.softcenter.cra.utils.date.DateUtils;

/**
 * @author Rodrigo Junior Utiyama
 * @since  30-09-2015
 */

public class PortadorDAOImpl implements DAOModel<BeanPortador, Integer>{

	private PreparedStatement pstmt;
	private Connection conn;
	private Statement stmt;
	
	public PortadorDAOImpl() {
		try{
			conn = FirebirdDatabaseConfig.getConfig().getConnection();
			stmt = conn.createStatement();
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}
	}
	
	@Override
	public boolean insert(BeanPortador portador) {
		String sql =  " INSERT INTO portador (codportador, agencia, nomeportador, "
					+ " 	agcentraliz, datacadastro, indativo, codmunicipio, endereco, cep, uf, cnpj) "
					+ " VALUES (?,?,?,?,?,?,?,?,?,?,?)";
		
		try{
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, portador.getCodigo());
			pstmt.setInt(2, portador.getAgencia());
			pstmt.setString(3, portador.getNome());
			pstmt.setString(4, portador.getAgenciaCentralizadora());
			pstmt.setDate(5, DateUtils.ConvertUtilToSQLDate(portador.getDataCadastro()));
			pstmt.setString(6, (portador.isAtivo() ? "S" : "N"));
			pstmt.setString(7, portador.getCodigoMunicipio());
			pstmt.setString(8, portador.getEndereco());
			pstmt.setString(9, portador.getCep());
			pstmt.setString(10,  portador.getUf());
			pstmt.setString(11, portador.getCpnj());
			
			return pstmt.execute();
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean delete(BeanPortador portador) {
		return false;
	}

	@Override
	public boolean update(BeanPortador portador, Integer codigo) {
		return false;
	}

	@Override
	public BeanPortador select(Integer codigo) {
		String sql = "SELECT * FROM portador WHERE codportador = ?";
		
		try{
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, codigo);
		
			ResultSet rs = pstmt.executeQuery();
			
			if(rs.next()){
				BeanPortador portador = new BeanPortador();
				portador.setAgencia(rs.getInt("AGENCIA"));
				portador.setAgenciaCentralizadora(rs.getString("AGCENTRALIZ"));
				portador.setAtivo( (rs.getString("INDATIVO").equals("S")) ? true : false );
				portador.setCep(rs.getString("CEP"));
				portador.setCodigo(rs.getInt("CODPORTADOR"));
				portador.setCodigoMunicipio(rs.getString("CODMUNICIPIO"));
				portador.setCpnj(rs.getString("CNPJ"));
				portador.setDataCadastro(rs.getDate("DATACADASTRO"));
				portador.setEndereco(rs.getString("ENDERECO"));
				portador.setNome(rs.getString("NOMEPORTADOR"));
				portador.setUf(rs.getString("UF"));
				
				return portador;
			}
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}
		return null;
	}

	@Override
	public List<BeanPortador> select() {
		String sql = "SELECT * FROM portador";
		
		try{
			pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();
			List<BeanPortador> portadores = new ArrayList<BeanPortador>();
			
			while(rs.next()){
				BeanPortador portador = new BeanPortador();
				portador.setAgencia(rs.getInt("AGENCIA"));
				portador.setAgenciaCentralizadora(rs.getString("AGCENTRALIZ"));
				portador.setAtivo( (rs.getString("INDATIVO").equals("S")) ? true : false );
				portador.setCep(rs.getString("CEP"));
				portador.setCodigo(rs.getInt("CODPORTADOR"));
				portador.setCodigoMunicipio(rs.getString("CODMUNICIPIO"));
				portador.setCpnj(rs.getString("CNPJ"));
				portador.setDataCadastro(rs.getDate("DATACADASTRO"));
				portador.setEndereco(rs.getString("ENDERECO"));
				portador.setNome(rs.getString("NOMEPORTADOR"));
				portador.setUf(rs.getString("UF"));
				portadores.add(portador);
			}
			
			return portadores;
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}
		return null;
	}
 
	@Override
	public int getLastRecordCount() throws SQLException {
		// TODO Auto-generated method stub
		return 0;
	}
}
