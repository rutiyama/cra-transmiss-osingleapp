package br.com.softcenter.cra.model.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.com.softcenter.cra.model.beans.BeanMunicipio;
import br.com.softcenter.cra.model.beans.BeanUsuario;
import br.com.softcenter.cra.model.beans.Perfil;
import br.com.softcenter.cra.model.dao.connection.FirebirdDatabaseConfig;
import br.com.softcenter.cra.utils.date.DateUtils;

/**
 * @author Rodrigo Junior Utiyama
 * @since  30-09-2015
 */

public class UsuarioDAOImpl implements DAOModel<BeanUsuario, Integer>{
	
	private PreparedStatement pstmt;
	private Connection conn;
	private Statement stmt;
	
	public UsuarioDAOImpl(){
		try{
			conn = FirebirdDatabaseConfig.getConfig().getConnection();
			stmt = conn.createStatement();
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}
	}
	
	@Override
	public boolean insert(BeanUsuario usuario) {
		
		String sql = "INSERT INTO usuarios (codigo, login, senha, datacadastro,codperfil,codmunicipio,  "
				+ "status, indativo) "
					+ "VALUES (?,?,?,?,?,?,?,?,?) ";
		Character indativo = (usuario.getIsAtivo()) ? 'S' : 'N';
	
		try{ 
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, getLastRecordCount());
			pstmt.setString(2, usuario.getLogin());
			pstmt.setString(3, usuario.getSenha());
			pstmt.setDate(4, DateUtils.ConvertUtilToSQLDate(usuario.getDataCadastro()));
			pstmt.setInt(5,usuario.getPerfilUsuario().getCodigo());
			pstmt.setInt(6, usuario.getMunicipio().getCodigo());
			pstmt.setString(7, usuario.getStatus().toString());
			pstmt.setString(8, indativo.toString());
			pstmt.setString(9, usuario.getPathArqRemessa());
			
			pstmt.executeQuery();
 			pstmt.close();

 			return true;
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean delete(BeanUsuario usuario) {
		String sql = "DELETE FROM usuario WHERE codigo = ?";
		
		try{
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, usuario.getCodigo());
			pstmt.executeQuery();
			pstmt.close();
			
			return true;
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		}
		
		return false;
	}

	@Override
	public boolean update(BeanUsuario usuario, Integer codigo) {

		String sql = "UPDATE usuario";
		return false;
	}

	@Override
	public BeanUsuario select(Integer codigo) {
		String sql = " SELECT  usrs.codigo AS codigousuario, usrs.login, usrs.senha, " +
					 " 	usrs.datacadastro AS datacadastrousuario, " +
				     "	usrs.codperfil, usrs.codmunicipio, usrs.status, usrs.indativo AS indativousuario, "  +
					 "  usrs.patharqremessa AS pathremessa," +
				     "	muni.codigo AS codigomunicipio, muni.nome, muni.codestado, muni.codmunipagto, "  +
				     "	muni.datacadastro AS datacadastromunicipio, "  +
				     "	muni.indativo AS indativomunicipio, muni.tipopagto, muni.tipoenvio, "  +
				     "	perfis.codigo AS codigoperfil, perfis.descricao, "  +
				     "	perfis.datacadastro AS datacadastroperfil, perfis.indativo AS indativoperfil "  +
				     "		FROM usuarios usrs "  +
				     "	LEFT JOIN municipios muni ON (muni.codigo = usrs.codmunicipio) "  +
				     "	LEFT JOIN perfisusuarios perfis ON (perfis.codigo = usrs.codigo) "  +
        			 "	WHERE usrs.codigo = ? and usrs.indativo = 'S'";	
		
		return executarSelectBeanUsuario(sql, codigo).get(0);
	}

	private void popularPerfilUsuario(Perfil perfilUsuario, ResultSet rs) throws SQLException{
		perfilUsuario.setCodigo(rs.getInt("CODIGOPERFIL"));
		perfilUsuario.setDataCadastro(rs.getDate("DATACADASTROPERFIL"));
		perfilUsuario.setDescricao(rs.getString("DESCRICAO"));
		perfilUsuario.setIsAtivo((rs.getString("INDATIVOPERFIL") != null && rs.getString("INDATIVOPERFIL").equals("S") ? true : false));
	}
	
	private void popularMunicipio(BeanMunicipio beanMunicipio, ResultSet rs) throws SQLException{
		beanMunicipio.setCodigo(rs.getInt("CODMUNICIPIO"));
		beanMunicipio.setDataCadastro(rs.getDate("DATACADASTROMUNICIPIO"));
		beanMunicipio.setIndAtivo(rs.getString("INDATIVOMUNICIPIO").toCharArray()[0]);
		beanMunicipio.setNome(rs.getString("NOME"));
		beanMunicipio.setTipoEnvio(rs.getString("TIPOENVIO").toCharArray()[0]);
		beanMunicipio.setTipoPagamento(rs.getString("TIPOPAGTO").toCharArray()[0]);
	}
	 
	public BeanUsuario select(Date dataCadastro) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BeanUsuario> select() {
		String sql = " SELECT  usrs.codigo AS codigousuario, usrs.login, usrs.senha, " 					+
				 " 	usrs.datacadastro AS datacadastrousuario, usrs.patharqremessa AS pathremessa, "     +
			     "	usrs.codperfil, usrs.codmunicipio, usrs.status, usrs.indativo AS indativousuario, " +
			     "	muni.codigo AS codigomunicipio, muni.nome, muni.codestado, muni.codmunipagto, "  	+
			     "	muni.datacadastro AS datacadastromunicipio, "  										+
			     "	muni.indativo AS indativomunicipio, muni.tipopagto, muni.tipoenvio, "  				+
			     "	perfis.codigo AS codigoperfil, perfis.descricao, "  								+
			     "	perfis.datacadastro AS datacadastroperfil, perfis.indativo AS indativoperfil "  	+
			     "		FROM usuarios usrs "  															+
			     "	LEFT JOIN municipios muni ON (muni.codigo = usrs.codmunicipio) "   					+
			     "	LEFT JOIN perfisusuarios perfis ON (perfis.codigo = usrs.codigo) " 					+ 
			     "		WHERE usrs.indativo = 'S' ORDER BY usrs.codigo ASC ";	
		
		//Parâmetro -1 significa para selecionar todos os usuarios:
		return executarSelectBeanUsuario(sql, -1);
	}
 
	public List<BeanUsuario> select(Character tipoPerfil) {
		String sql = " SELECT  usrs.codigo AS codigousuario, usrs.login, usrs.senha, " 					+
				 " 	usrs.datacadastro AS datacadastrousuario, usrs.patharqremessa AS pathremessa, "		+
			     "	usrs.codperfil, usrs.codmunicipio, usrs.status, usrs.indativo AS indativousuario, " +
			     "	muni.codigo AS codigomunicipio, muni.nome, muni.codestado, muni.codmunipagto, "  	+
			     "	muni.datacadastro AS datacadastromunicipio, "  										+
			     "	muni.indativo AS indativomunicipio, muni.tipopagto, muni.tipoenvio, "  				+
			     "	perfis.codigo AS codigoperfil, perfis.descricao, "  								+
			     "	perfis.datacadastro AS datacadastroperfil, perfis.indativo AS indativoperfil "  	+
			     "		FROM usuarios usrs "  															+
			     "	LEFT JOIN municipios muni ON (muni.codigo = usrs.codmunicipio) "   					+
			     "	LEFT JOIN perfisusuarios perfis ON (perfis.codigo = usrs.codigo) " 					+ 
			     "		WHERE usrs.indativo = 'S' ORDER BY usrs.codigo ASC ";	
		
		
		return null;
	}

	@Override
	public int getLastRecordCount() throws SQLException{
		ResultSet rs = null;
		String sql = "SELECT COUNT(codigo) AS quantidadeRegistros FROM usuarios";
		
		try{ 
			this.stmt.execute(sql);
			rs = this.stmt.getResultSet();
			rs.next();
			return (rs.getInt("quantidadeRegistros") + 1);
		}catch(Exception e){
			throw new SQLException();
		}
	}
	
	private List<BeanUsuario> executarSelectBeanUsuario(String sql, int codigo){
		ResultSet rs 				= null;
		BeanUsuario beanUsuario 	= null;
		BeanMunicipio beanMunicipio = null;
		Perfil perfilUsuario 		= null;
		List<BeanUsuario> beanusuarioList = new ArrayList<BeanUsuario>();
		
		try{
			pstmt = conn.prepareStatement(sql);
			
			//Se caso vier um código negativo, significa que é para selecionar todos os usuarios:
			if (codigo >= 0){
				pstmt.setInt(1, codigo);
			}
			
			if (pstmt.execute()){
				rs = pstmt.getResultSet();
				while (rs.next()){
					//Cria os objetos para serem populados (JDBC):
					beanUsuario 	= new BeanUsuario();
					beanMunicipio 	= new BeanMunicipio();
					
					//Para o perfil Usuario, é criado conforme o código do perfil, seguindo o padrão Factory:
					perfilUsuario = PerfilUsuarioFactory.getPerfilUsuario(rs.getInt("CODPERFIL"));
					
					//Popula o objeto do tipo BeanUsuario
					beanUsuario.setCodigo(rs.getInt("CODIGOUSUARIO"));
					beanUsuario.setDataCadastro(rs.getDate("DATACADASTROUSUARIO"));
					beanUsuario.setIsAtivo((rs.getString("INDATIVO").equals("S")) ? true : false);
					beanUsuario.setLogin(rs.getString("LOGIN"));
					beanUsuario.setPathArqRemessa(rs.getString("PATHREMESSA"));
					
					//Popula o Objeto do tipo BeanMunicipio, para setar a composi��o no Usuario:
					popularMunicipio(beanMunicipio, rs);
					beanUsuario.setMunicipio(beanMunicipio);
					
					//Popula o Objeto do tipo PerfilUsuario:
					popularPerfilUsuario(perfilUsuario, rs);
					beanUsuario.setPerfilUsuario(perfilUsuario);
					
					beanUsuario.setSenha(rs.getString("SENHA"));
					beanUsuario.setStatus(rs.getString("STATUS").toCharArray()[0]);
					
					beanusuarioList.add(beanUsuario);
				}
				
				pstmt.close();
				rs.close();
				
				return beanusuarioList;
			}
		}catch(SQLException sqlex){
			sqlex.printStackTrace();
		} 
		
		return null;
	}
}
