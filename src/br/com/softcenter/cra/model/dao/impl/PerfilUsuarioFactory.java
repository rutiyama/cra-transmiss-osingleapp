package br.com.softcenter.cra.model.dao.impl;

import br.com.softcenter.cra.model.beans.BeanPerfilAdministrador;
import br.com.softcenter.cra.model.beans.BeanPerfilMaster;
import br.com.softcenter.cra.model.beans.BeanPerfilUsuario;
import br.com.softcenter.cra.model.beans.Perfil;

public class PerfilUsuarioFactory {
	public static Perfil getPerfilUsuario(int codigo){
		switch (codigo){
			case 1 : return new BeanPerfilUsuario();
			case 2 : return new BeanPerfilAdministrador();
			case 3 : return new BeanPerfilMaster();
		}
		
		return null;
	}
}
