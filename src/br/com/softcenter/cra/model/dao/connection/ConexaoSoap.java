package br.com.softcenter.cra.model.dao.connection;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.axis.AxisFault;
import br.com.cra21.crapr.ServerCraBindingStub;
import br.com.softcenter.cra.utils.arquivoini.PropertyFile;

/**
 * @author Rodrigo Junior Utiyama
 * @since 01-09-2015
 */


public class ConexaoSoap {

	private static URL url;
	private static ServerCraBindingStub stub;
	
	private ConexaoSoap(){}
	
	public static ServerCraBindingStub getServerConnection(String usuario, String senha) throws AxisFault, MalformedURLException{
		if(stub == null){
			url = new URL(PropertyFile.getProperty().getConfig().getSoap_url());
			stub = new ServerCraBindingStub(url, null);
		}
		iniciarConexao(usuario, senha);
		return stub;
	}
	
	private static void iniciarConexao(String usuario, String senha) throws MalformedURLException, AxisFault {
		stub.setUsername(usuario);
		stub.setPassword(senha);
	}
}
