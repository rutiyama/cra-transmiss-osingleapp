package br.com.softcenter.cra.model.beans;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Rodrigo Junior Utiyama
 * @since  26-08-2015
 */


public class BeanUsuario implements Serializable{
	private int codigo;
	private String login;
	private String senha;
	private Date dataCadastro;
	private Boolean isAtivo;
	private Perfil perfilUsuario;
	private Character status;
	private BeanMunicipio municipio;
	private String pathArqRemessa;
	
	public BeanUsuario(){}
	
	public BeanUsuario(int codigo, String login, String senha, Date dataCadastro, Boolean isAtivo, Perfil perfilUsuario,
			Character status, BeanMunicipio municipio, String pathArqRemessa) {
		super();
		this.codigo = codigo;
		this.login = login;
		this.senha = senha;
		this.dataCadastro = dataCadastro;
		this.isAtivo = isAtivo;
		this.perfilUsuario = perfilUsuario;
		this.status = status;
		this.municipio = municipio;
		this.pathArqRemessa = pathArqRemessa;
	}
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public Date getDataCadastro() {
		return dataCadastro;
	}
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}
	public Boolean getIsAtivo() {
		return isAtivo;
	}
	public void setIsAtivo(Boolean isAtivo) {
		this.isAtivo = isAtivo;
	}
	public Perfil getPerfilUsuario() {
		return perfilUsuario;
	}
	public void setPerfilUsuario(Perfil perfilUsuario) {
		this.perfilUsuario = perfilUsuario;
	}
	public Character getStatus() {
		return status;
	}
	public void setStatus(Character status) {
		this.status = status;
	}
	public BeanMunicipio getMunicipio() {
		return municipio;
	}
	public void setMunicipio(BeanMunicipio municipio) {
		this.municipio = municipio;
	}
	public String getPathArqRemessa() {
		return pathArqRemessa;
	}
	public void setPathArqRemessa(String pathArqRemessa) {
		this.pathArqRemessa = pathArqRemessa;
	}
	@Override
	public String toString() {
		return "BeanUsuario [codigo=" + codigo + ", login=" + login + ", senha=" + senha + ", dataCadastro="
				+ dataCadastro + ", isAtivo=" + isAtivo + ", perfilUsuario=" + perfilUsuario + ", status=" + status
				+ ", municipio=" + municipio + ", pathArqRemessa=" + pathArqRemessa + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + codigo;
		result = prime * result + ((dataCadastro == null) ? 0 : dataCadastro.hashCode());
		result = prime * result + ((isAtivo == null) ? 0 : isAtivo.hashCode());
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		result = prime * result + ((municipio == null) ? 0 : municipio.hashCode());
		result = prime * result + ((pathArqRemessa == null) ? 0 : pathArqRemessa.hashCode());
		result = prime * result + ((perfilUsuario == null) ? 0 : perfilUsuario.hashCode());
		result = prime * result + ((senha == null) ? 0 : senha.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BeanUsuario other = (BeanUsuario) obj;
		if (codigo != other.codigo)
			return false;
		if (dataCadastro == null) {
			if (other.dataCadastro != null)
				return false;
		} else if (!dataCadastro.equals(other.dataCadastro))
			return false;
		if (isAtivo == null) {
			if (other.isAtivo != null)
				return false;
		} else if (!isAtivo.equals(other.isAtivo))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (municipio == null) {
			if (other.municipio != null)
				return false;
		} else if (!municipio.equals(other.municipio))
			return false;
		if (pathArqRemessa == null) {
			if (other.pathArqRemessa != null)
				return false;
		} else if (!pathArqRemessa.equals(other.pathArqRemessa))
			return false;
		if (perfilUsuario == null) {
			if (other.perfilUsuario != null)
				return false;
		} else if (!perfilUsuario.equals(other.perfilUsuario))
			return false;
		if (senha == null) {
			if (other.senha != null)
				return false;
		} else if (!senha.equals(other.senha))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	
}
