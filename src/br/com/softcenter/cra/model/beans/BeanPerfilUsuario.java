package br.com.softcenter.cra.model.beans;

/**
 * @author Rodrigo Junior Utiyama
 * @since  26-08-2015
 */

public class BeanPerfilUsuario extends Perfil {
	
	public BeanPerfilUsuario() {
		super.tipoPerfil = 'U';
	}

	@Override
	public Character getTipoPerfil() {
		return super.tipoPerfil;
	}
}
