package br.com.softcenter.cra.model.beans.remessa;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Rodrigo Junior Utiyama
 * @since 02-10-2015
 */

public class BeanRemessaTrailler {
	private String codigoRegistro;
	private String codigoPortador;
	private String nomePortador;
	private String dataMovimento;
	private String quantidadeNaRemessa;
	private String valorRemessa;
	private String complementoRegistro;
	private String sequenciaRegistroNoArquivo;
	public String getCodigoRegistro() {
		return codigoRegistro;
	}
	public void setCodigoRegistro(String codigoRegistro) {
		this.codigoRegistro = codigoRegistro;
	}
	public String getCodigoPortador() {
		return codigoPortador;
	}
	public void setCodigoPortador(String codigoPortador) {
		this.codigoPortador = codigoPortador;
	}
	public String getNomePortador() {
		return nomePortador;
	}
	public void setNomePortador(String nomePortador) {
		this.nomePortador = nomePortador;
	}
	public String getDataMovimento() {
		return dataMovimento;
	}
	public void setDataMovimento(String dataMovimento) {
		this.dataMovimento = dataMovimento;
	}
	public String getQuantidadeNaRemessa() {
		return quantidadeNaRemessa;
	}
	public void setQuantidadeNaRemessa(String quantidadeNaRemessa) {
		this.quantidadeNaRemessa = quantidadeNaRemessa;
	}
	public String getValorRemessa() {
		return valorRemessa;
	}
	public void setValorRemessa(String valorRemessa) {
		this.valorRemessa = valorRemessa;
	}
	public String getComplementoRegistro() {
		return complementoRegistro;
	}
	public void setComplementoRegistro(String complementoRegistro) {
		this.complementoRegistro = complementoRegistro;
	}
	public String getSequenciaRegistroNoArquivo() {
		return sequenciaRegistroNoArquivo;
	}
	public void setSequenciaRegistroNoArquivo(String sequenciaRegistroNoArquivo) {
		this.sequenciaRegistroNoArquivo = sequenciaRegistroNoArquivo;
	}
	@Override
	public String toString() {
		return "BeanRemessaTrailler [codigoRegistro=" + codigoRegistro + ", codigoPortador=" + codigoPortador
				+ ", nomePortador=" + nomePortador + ", dataMovimento=" + dataMovimento + ", quantidadeNaRemessa="
				+ quantidadeNaRemessa + ", valorRemessa=" + valorRemessa + ", complementoRegistro="
				+ complementoRegistro + ", sequenciaRegistroNoArquivo=" + sequenciaRegistroNoArquivo + "]";
	}

	
}
