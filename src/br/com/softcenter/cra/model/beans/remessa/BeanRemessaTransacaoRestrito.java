package br.com.softcenter.cra.model.beans.remessa;

/**
 * @author Rodrigo Junior Utiyama
 * @since 02-10-2015
 */
public class BeanRemessaTransacaoRestrito {
	private String	codigoCartorio;
	private	String	numeroProtocoloCartorio;
	private	String	tipoOcorrencia;
	private String	dataProtocolo;
	private String	valorCustasCartorio;
	private String	dataOcorrencia;
	private String	codigoIrregularidade;
	private String	valorCustasCartorioDistribuidor;//
	private String	registroDistruicao;//
	private String	complementoCodigoIrregularidade;//
	private String	valorDemaisDespesasSedexCpmf;
	private String  valorDemaisDespesas;
	
	public String getCodigoCartorio() {
		return codigoCartorio;
	}
	public void setCodigoCartorio(String codigoCartorio) {
		this.codigoCartorio = codigoCartorio;
	}
	public String getNumeroProtocoloCartorio() {
		return numeroProtocoloCartorio;
	}
	public void setNumeroProtocoloCartorio(String numeroProtocoloCartorio) {
		this.numeroProtocoloCartorio = numeroProtocoloCartorio;
	}
	public String getTipoOcorrencia() {
		return tipoOcorrencia;
	}
	public void setTipoOcorrencia(String tipoOcorrencia) {
		this.tipoOcorrencia = tipoOcorrencia;
	}
	public String getDataProtocolo() {
		return dataProtocolo;
	}
	public void setDataProtocolo(String dataProtocolo) {
		this.dataProtocolo = dataProtocolo;
	}
	public String getValorCustasCartorio() {
		return valorCustasCartorio;
	}
	public void setValorCustasCartorio(String valorCustasCartorio) {
		this.valorCustasCartorio = valorCustasCartorio;
	}
	public String getDataOcorrencia() {
		return dataOcorrencia;
	}
	public void setDataOcorrencia(String dataOcorrencia) {
		this.dataOcorrencia = dataOcorrencia;
	}
	public String getCodigoIrregularidade() {
		return codigoIrregularidade;
	}
	public void setCodigoIrregularidade(String codigoIrregularidade) {
		this.codigoIrregularidade = codigoIrregularidade;
	}
	public String getValorCustasCartorioDistribuidor() {
		return valorCustasCartorioDistribuidor;
	}
	public void setValorCustasCartorioDistribuidor(String valorCustasCartorioDistribuidor) {
		this.valorCustasCartorioDistribuidor = valorCustasCartorioDistribuidor;
	}
	public String getRegistroDistruicao() {
		return registroDistruicao;
	}
	public void setRegistroDistruicao(String registroDistruicao) {
		this.registroDistruicao = registroDistruicao;
	}
	public String getComplementoCodigoIrregularidade() {
		return complementoCodigoIrregularidade;
	}
	public void setComplementoCodigoIrregularidade(String complementoCodigoIrregularidade) {
		this.complementoCodigoIrregularidade = complementoCodigoIrregularidade;
	}
	public String getValorDemaisDespesasSedexCpmf() {
		return valorDemaisDespesasSedexCpmf;
	}
	public void setValorDemaisDespesasSedexCpmf(String valorDemaisDespesasSedexCpmf) {
		this.valorDemaisDespesasSedexCpmf = valorDemaisDespesasSedexCpmf;
	}
	public String getValorDemaisDespesas() {
		return valorDemaisDespesas;
	}
	public void setValorDemaisDespesas(String valorDemaisDespesas) {
		this.valorDemaisDespesas = valorDemaisDespesas;
	}
}
