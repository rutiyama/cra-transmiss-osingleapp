package br.com.softcenter.cra.model.beans.remessa;

/**
 * @author Rodrigo Junior Utiyama
 * @since 02-10-2015
 */
public class BeanRemessaTransacaoBanco {
	private String	numeroOperacaoBanco;
	private String	numeroContratoBanco;
	private	String	numeroParcelaDoContrato;
	private String	tipoLetraCambio;
	
	public BeanRemessaTransacaoBanco() {
	}

	public String getNumeroOperacaoBanco() {
		return numeroOperacaoBanco;
	}
	public void setNumeroOperacaoBanco(String numeroOperacaoBanco) {
		this.numeroOperacaoBanco = numeroOperacaoBanco;
	}
	public String getNumeroContratoBanco() {
		return numeroContratoBanco;
	}
	public void setNumeroContratoBanco(String numeroContratoBanco) {
		this.numeroContratoBanco = numeroContratoBanco;
	}
	public String getNumeroParcelaDoContrato() {
		return numeroParcelaDoContrato;
	}
	public void setNumeroParcelaDoContrato(String numeroParcelaDoContrato) {
		this.numeroParcelaDoContrato = numeroParcelaDoContrato;
	}
	public String getTipoLetraCambio() {
		return tipoLetraCambio;
	}
	public void setTipoLetraCambio(String tipoLetraCambio) {
		this.tipoLetraCambio = tipoLetraCambio;
	}
	
	
	
}
