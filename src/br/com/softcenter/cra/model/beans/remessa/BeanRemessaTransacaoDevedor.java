package br.com.softcenter.cra.model.beans.remessa;
/**
 * @author Rodrigo Junior Utiyama
 * @since 02-10-2015
 */

public class BeanRemessaTransacaoDevedor {
	private String	numeroControleDevedor;
	private String	nomeDevedor;
	private String	tipoIdentificacaoDevedor;
	private String	numeroIdentificacaoDevedor;
	private	String	documentoDevedor;
	private String	enderecoDevedor;
	private	String	cepDevedor;
	private String	cidadeDevedor;
	private	String	ufDevedor;
	private String	bairroDevedor;//
	
	public String getNumeroControleDevedor() {
		return numeroControleDevedor;
	}
	public void setNumeroControleDevedor(String numeroControleDevedor) {
		this.numeroControleDevedor = numeroControleDevedor;
	}
	public String getNomeDevedor() {
		return nomeDevedor;
	}
	public void setNomeDevedor(String nomeDevedor) {
		this.nomeDevedor = nomeDevedor;
	}
	public String getTipoIdentificacaoDevedor() {
		return tipoIdentificacaoDevedor;
	}
	public void setTipoIdentificacaoDevedor(String tipoIdentificacaoDevedor) {
		this.tipoIdentificacaoDevedor = tipoIdentificacaoDevedor;
	}
	public String getNumeroIdentificacaoDevedor() {
		return numeroIdentificacaoDevedor;
	}
	public void setNumeroIdentificacaoDevedor(String numeroIdentificacaoDevedor) {
		this.numeroIdentificacaoDevedor = numeroIdentificacaoDevedor;
	}
	public String getDocumentoDevedor() {
		return documentoDevedor;
	}
	public void setDocumentoDevedor(String documentoDevedor) {
		this.documentoDevedor = documentoDevedor;
	}
	public String getEnderecoDevedor() {
		return enderecoDevedor;
	}
	public void setEnderecoDevedor(String enderecoDevedor) {
		this.enderecoDevedor = enderecoDevedor;
	}
	public String getCepDevedor() {
		return cepDevedor;
	}
	public void setCepDevedor(String cepDevedor) {
		this.cepDevedor = cepDevedor;
	}
	public String getCidadeDevedor() {
		return cidadeDevedor;
	}
	public void setCidadeDevedor(String cidadeDevedor) {
		this.cidadeDevedor = cidadeDevedor;
	}
	public String getUfDevedor() {
		return ufDevedor;
	}
	public void setUfDevedor(String ufDevedor) {
		this.ufDevedor = ufDevedor;
	}
	public String getBairroDevedor() {
		return bairroDevedor;
	}
	public void setBairroDevedor(String bairroDevedor) {
		this.bairroDevedor = bairroDevedor;
	}
	@Override
	public String toString() {
		return "BeanRemessaTransacaoDevedor [numeroControleDevedor=" + numeroControleDevedor + ", nomeDevedor="
				+ nomeDevedor + ", tipoIdentificacaoDevedor=" + tipoIdentificacaoDevedor
				+ ", numeroIdentificacaoDevedor=" + numeroIdentificacaoDevedor + ", documentoDevedor="
				+ documentoDevedor + ", enderecoDevedor=" + enderecoDevedor + ", cepDevedor=" + cepDevedor
				+ ", cidadeDevedor=" + cidadeDevedor + ", ufDevedor=" + ufDevedor + ", bairroDevedor=" + bairroDevedor
				+ "]";
	}
}
