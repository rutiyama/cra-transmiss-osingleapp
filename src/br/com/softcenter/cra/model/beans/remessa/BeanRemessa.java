package br.com.softcenter.cra.model.beans.remessa;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rodrigo Junior Utiyama
 * @since 02-10-2015
 */

public class BeanRemessa {
	private BeanRemessaHeader 			header;
	private List<BeanRemessaTransacao>  transacoes;
	private BeanRemessaTrailler 		trailler;
	
	public BeanRemessa(){
		header 		= new BeanRemessaHeader();
		transacoes 	= new ArrayList<BeanRemessaTransacao>();
		trailler 	= new BeanRemessaTrailler();
	}
	
	public BeanRemessaHeader getHeader() {
		return header;
	}
	public void setHeader(BeanRemessaHeader header) {
		this.header = header;
	}
	public BeanRemessaTrailler getTrailler() {
		return trailler;
	}
	public void setTrailler(BeanRemessaTrailler trailler) {
		this.trailler = trailler;
	}
	public List<BeanRemessaTransacao> getTransacoes() {
		return transacoes;
	}
	public void setTransacoes(List<BeanRemessaTransacao> transacoes) {
		this.transacoes = transacoes;
	}

	@Override
	public String toString() {
		return "BeanRemessa [header=" + header + ", transacoes=" + transacoes + ", trailler=" + trailler + "]";
	}
	
	
}
