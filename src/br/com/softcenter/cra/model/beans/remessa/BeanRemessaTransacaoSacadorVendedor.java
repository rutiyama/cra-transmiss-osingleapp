package br.com.softcenter.cra.model.beans.remessa;

/**
 * @author Rodrigo Junior Utiyama
 * @since 02-10-2015
 */
public class BeanRemessaTransacaoSacadorVendedor {
	private String 		nomeSacadorOuVendedor;
	private String 		documentoSacador;
	private String 		enderecoSacadorOuVendedor;
	private String    	cepSacadorOuVendedor;
	private String 		cidadeSacadorOuVendedor;
	private String 		ufSacadorOuVendedor;
	
	public String getNomeSacadorOuVendedor() {
		return nomeSacadorOuVendedor;
	}
	public void setNomeSacadorOuVendedor(String nomeSacadorOuVendedor) {
		this.nomeSacadorOuVendedor = nomeSacadorOuVendedor;
	}
	public String getDocumentoSacador() {
		return documentoSacador;
	}
	public void setDocumentoSacador(String documentoSacador) {
		this.documentoSacador = documentoSacador;
	}
	public String getEnderecoSacadorOuVendedor() {
		return enderecoSacadorOuVendedor;
	}
	public void setEnderecoSacadorOuVendedor(String enderecoSacadorOuVendedor) {
		this.enderecoSacadorOuVendedor = enderecoSacadorOuVendedor;
	}
	public String getCepSacadorOuVendedor() {
		return cepSacadorOuVendedor;
	}
	public void setCepSacadorOuVendedor(String cepSacadorOuVendedor) {
		this.cepSacadorOuVendedor = cepSacadorOuVendedor;
	}
	public String getCidadeSacadorOuVendedor() {
		return cidadeSacadorOuVendedor;
	}
	public void setCidadeSacadorOuVendedor(String cidadeSacadorOuVendedor) {
		this.cidadeSacadorOuVendedor = cidadeSacadorOuVendedor;
	}
	public String getUfSacadorOuVendedor() {
		return ufSacadorOuVendedor;
	}
	public void setUfSacadorOuVendedor(String ufSacadorOuVendedor) {
		this.ufSacadorOuVendedor = ufSacadorOuVendedor;
	}
	
	
}
