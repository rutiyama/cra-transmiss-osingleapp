package br.com.softcenter.cra.model.beans.remessa;

/**
 * @author Rodrigo Junior Utiyama
 * @since 02-10-2015
 */
public class BeanRemessaTransacaoCedente {
	private String 		agenciaOuCodigoCedente;
	private String 		nomeCedenteOuFavorecido;
	
	public String getAgenciaOuCodigoCedente() {
		return agenciaOuCodigoCedente;
	}
	public void setAgenciaOuCodigoCedente(String agenciaOuCodigoCedente) {
		this.agenciaOuCodigoCedente = agenciaOuCodigoCedente;
	}
	public String getNomeCedenteOuFavorecido() {
		return nomeCedenteOuFavorecido;
	}
	public void setNomeCedenteOuFavorecido(String nomeCedenteOuFavorecido) {
		this.nomeCedenteOuFavorecido = nomeCedenteOuFavorecido;
	}
	
	
	
}
