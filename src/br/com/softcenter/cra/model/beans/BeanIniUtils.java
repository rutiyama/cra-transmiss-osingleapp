package br.com.softcenter.cra.model.beans;

import java.io.Serializable;

/**
 * @author Rodrigo Junior Utiyama
 * @since 26-08-2015
 */
public class BeanIniUtils implements Serializable{
	private String database_url;
	private String database_user;
	private String database_pass;
	private String database_encoding;
	private String soap_url;

	public BeanIniUtils(){}

	
	public BeanIniUtils(String database_url, String database_user, String database_pass, String database_encoding,
			String soap_url) {
		super();
		this.database_url = database_url;
		this.database_user = database_user;
		this.database_pass = database_pass;
		this.database_encoding = database_encoding;
		this.soap_url = soap_url;
	}


	public String getDatabase_url() {
		return database_url;
	}

	public void setDatabase_url(String database_url) {
		this.database_url = database_url;
	}

	public String getDatabase_user() {
		return database_user;
	}

	public void setDatabase_user(String database_user) {
		this.database_user = database_user;
	}

	public String getDatabase_pass() {
		return database_pass;
	}

	public void setDatabase_pass(String database_pass) {
		this.database_pass = database_pass;
	}

	public String getDatabase_encoding() {
		return database_encoding;
	}

	public void setDatabase_encoding(String database_encoding) {
		this.database_encoding = database_encoding;
	}

	public String getSoap_url() {
		return soap_url;
	}

	public void setSoap_url(String soap_url) {
		this.soap_url = soap_url;
	}
	
	
	
	 
}
