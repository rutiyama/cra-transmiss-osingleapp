package br.com.softcenter.cra.model.beans;

public class BeanPerfilAdministrador extends Perfil{

	public BeanPerfilAdministrador() {
		super.tipoPerfil = 'A';
	}
	
	@Override
	public Character getTipoPerfil() {
		return super.tipoPerfil;
	}

}
